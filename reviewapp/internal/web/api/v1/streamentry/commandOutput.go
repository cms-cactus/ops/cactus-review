package streamentry

// TypeCommandOutput contains output of the executing command
const TypeCommandOutput Type = "commandOutput"

// DataCommandOutput represents command output data, can be anything
type DataCommandOutput map[string]interface{}

// EntryCommandOutput creates a CommandOutput stream entry with given payload
func EntryCommandOutput(data DataCommandOutput) *Entry {
	return &Entry{TypeCommandOutput, data}
}
