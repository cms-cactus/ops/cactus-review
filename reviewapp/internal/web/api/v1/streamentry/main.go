package streamentry

// Type is the type of a StreamEntry
type Type string

// Entry is the type used to send out results over the API
type Entry struct {
	TypeName Type        `json:"type"`
	Data     interface{} `json:"data"`
}
