package streamentry

// TypeExitStatus marks the completion of a command
const TypeExitStatus Type = "exitStatus"

// DataExitStatus is the payload for a TypeExitStatus stream entry
type DataExitStatus struct {
	Success bool   `json:"success"`
	Error   string `json:"error,omitempty"`
}

// EntryExitStatus creates an ExitCode stream entry with given payload
func EntryExitStatus(exitStatus error) *Entry {
	success := exitStatus == nil
	err := ""
	if !success {
		err = exitStatus.Error()
	}
	return &Entry{TypeExitStatus, DataExitStatus{success, err}}
}
