package apiv1

import (
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/middleware"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
)

var log = logrus.WithField("package", "apiv1")

func injectLogger(logger *logrus.Logger) func(next http.Handler) http.Handler {
	return middleware.RequestLogger(&chiFormatter{})
}

func getLogger(r *http.Request) logrus.FieldLogger {
	entry := middleware.GetLogEntry(r).(*chiFormatterEntry)
	return entry.Logger
}

type chiFormatter struct {
}

func (l *chiFormatter) NewLogEntry(r *http.Request) middleware.LogEntry {
	onceFields := logrus.Fields{}
	entryFields := logrus.Fields{}

	correlationBytes, err := uuid.NewUUID()
	var correlationID string
	if err != nil {
		correlationID = "missing"
	} else {
		correlationID = correlationBytes.String()
	}
	entryFields["x_id"] = correlationID

	scheme := "http"
	if r.TLS != nil {
		scheme = "https"
	}
	onceFields["http_scheme"] = scheme
	onceFields["http_proto"] = r.Proto
	onceFields["http_method"] = r.Method

	onceFields["remote_addr"] = r.RemoteAddr
	onceFields["user_agent"] = r.UserAgent()

	onceFields["uri"] = fmt.Sprintf("%s://%s%s", scheme, r.Host, r.RequestURI)

	entry := &chiFormatterEntry{Logger: log.WithFields(entryFields)}

	entry.Logger.WithFields(onceFields).Infoln("request started")

	return entry
}

type chiFormatterEntry struct {
	Logger *logrus.Entry
}

func (l *chiFormatterEntry) Write(status, bytes int, headers http.Header, elapsed time.Duration, payload interface{}) {
	l.Logger = l.Logger.WithFields(logrus.Fields{
		"resp_status": status, "resp_bytes_length": bytes,
		"resp_elapsed_ms": float64(elapsed.Nanoseconds()) / 1000000.0,
	})

	l.Logger.Infoln("request complete")
}

func (l *chiFormatterEntry) Panic(v interface{}, stack []byte) {
	l.Logger = l.Logger.WithFields(logrus.Fields{
		"stack": string(stack),
		"panic": fmt.Sprintf("%+v", v),
	})
}
