package apiv1

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

func init() {
	v1router.Route("/review", func(r chi.Router) {
		r.Use(injectDeployments)
		r.Get("/", listDeployments)
		r.Route("/{projectname}/{branchname}", func(r chi.Router) {
			r.Use(injectDeployment)
			r.Get("/", redirectDeployment)
		})
	})
}

func listDeployments(w http.ResponseWriter, r *http.Request) {
	deployments, ok := r.Context().Value(deploymentsKey).([]deployment)
	if !ok {
		getLogger(r).Error("no deployments found in context")
		Error(500).Send(w, r)
		return
	}
	result := "<html><body><ul>"
	for _, d := range deployments {
		result += "<li><a href=\"" + getHref(r, d.Port) + "\">" + d.ProjectName + ":" + d.Branch + "</a></li>\n"
	}
	result += "</ul></body></html>\n"
	render.HTML(w, r, result)
}

func redirectDeployment(w http.ResponseWriter, r *http.Request) {
	deployment, ok := r.Context().Value(deploymentKey).(deployment)
	if !ok {
		Error(404).Send(w, r)
		return
	}
	if deployment.Port == 0 {
		ErrorMsg(500, "this deployment has no valid port assignment").Send(w, r)
		return
	}
	w.Header().Set("Location", getHref(r, deployment.Port))
	w.WriteHeader(307)
}

func getHref(r *http.Request, port int16) string {
	hostname := r.Host
	if i := strings.Index(r.Host, ":"); i >= 0 {
		hostname = hostname[0:i]
	}
	return fmt.Sprintf("http://%s:%v", hostname, port)
}
