package apiv1

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"os"
	"os/exec"
	"time"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
)

type deployment struct {
	ProjectName string `json:"projectname" yaml:"projectname"`
	Branch      string `json:"branch" yaml:"branch"`
	Port        int16  `json:"port" yaml:"port"`
}

type contextKey string

var deploymentKey contextKey = "deploycxt-deployment"
var deploymentsKey contextKey = "deploycxt-deployments"

func injectDeployments(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		deployments, err := fetchDeployments()
		if err != nil {
			getLogger(r).WithError(err).Error("cannot get deployments")
			code := 500
			if err.Error() == "time-out" {
				code = 504
			}
			Error(code).Send(w, r)
			return
		}
		cxt := context.WithValue(r.Context(), deploymentsKey, deployments)
		next.ServeHTTP(w, r.WithContext(cxt))
	})
}

func injectDeployment(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		deployments, ok := r.Context().Value(deploymentsKey).([]deployment)
		if !ok {
			getLogger(r).Error("no deployments found in context")
			Error(500).Send(w, r)
			return
		}
		var deployment *deployment
		projectName := chi.URLParam(r, "projectname")
		branchName := chi.URLParam(r, "branchname")
		deployment = deploymentFromProjectAndBranch(deployments, projectName, branchName)
		if deployment == nil {
			getLogger(r).WithFields(logrus.Fields{"projectName": projectName, "branch": branchName}).Error("not found")
			Error(404).Send(w, r)
		} else {
			cxt := context.WithValue(r.Context(), deploymentKey, *deployment)
			next.ServeHTTP(w, r.WithContext(cxt))
		}
	})
}

func deploymentFromProjectAndBranch(all []deployment, projectName string, branchName string) *deployment {
	for _, d := range all {
		if d.ProjectName == projectName && d.Branch == branchName {
			return &d
		}
	}
	return nil
}

type kubectlServicesResponse struct {
	Items []kubectlServiceResponse `json:"items"`
}

type kubectlServiceResponse struct {
	MetaData kubectlServiceMetadata `json:"metadata`
	Spec     kubectlServiceSpec     `json:"spec"`
}

type kubectlServiceMetadata struct {
	Labels map[string]string `json:"labels"`
}

type kubectlServiceSpec struct {
	Ports []kubectlServiceSpecPort `json:"ports"`
}

type kubectlServiceSpecPort struct {
	NodePort int `json:"nodeport"`
}

func fetchDeployments() ([]deployment, error) {
	cmd := exec.Command("kubectl", "--kubeconfig=kube.config", "get", "--all-namespaces", "services", "-o", "json")
	output := bytes.NewBuffer(nil)
	cmd.Stdout, cmd.Stderr = output, os.Stderr
	cmd.Start()

	timeout := time.After(10 * time.Second)
	done := make(chan error, 1)
	go func() {
		done <- cmd.Wait()
	}()

	select {
	case <-timeout:
		return nil, errors.New("time-out")
	case err := <-done:
		if err != nil {
			return nil, err
		}
	}

	var servicesResponse kubectlServicesResponse
	err := json.Unmarshal(output.Bytes(), &servicesResponse)
	if err != nil {
		return nil, err
	}
	result := make([]deployment, 0)
	for _, service := range servicesResponse.Items {
		project := service.MetaData.Labels["project"]
		branch := service.MetaData.Labels["branch"]
		port := 0
		if len(service.Spec.Ports) != 0 {
			port = service.Spec.Ports[0].NodePort
		}
		if project != "" && branch != "" && port != 0 {
			logrus.WithField("project", project).WithField("branch", branch).WithField("port", port).Info("found deployment")
			d := deployment{
				ProjectName: project,
				Branch:      branch,
				Port:        int16(port),
			}
			result = append(result, d)
		}
	}

	return result, nil
}
