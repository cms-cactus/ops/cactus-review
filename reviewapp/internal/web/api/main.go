package api

import (
	v1 "cactus-review/internal/web/api/v1"
	"fmt"
	"net"
	"net/http"

	"github.com/go-chi/chi"
)

// StartAndListen starts the API and opens a socket on the specified port
func StartAndListen(port string) error {

	r := chi.NewRouter()

	v1.Register(r)

	if port == "" {
		port = "0"
	}
	listener, err := net.Listen("tcp", ":"+port)
	if err != nil {
		return err
	}
	address := "http://" + listener.Addr().String()
	fmt.Println("listening on", address)
	return http.Serve(listener, r)
}
