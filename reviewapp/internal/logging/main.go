package logging

import (
	"os"

	log "github.com/sirupsen/logrus"
	"golang.org/x/crypto/ssh/terminal"
)

func InitLogging() {
	if !terminal.IsTerminal(int(os.Stdout.Fd())) {
		f := log.JSONFormatter{
			TimestampFormat: "2006-01-02 15:04:05",
		}
		log.SetFormatter(&f)
	} else {
		f := log.TextFormatter{
			TimestampFormat: "2006-01-02 15:04:05",
			FullTimestamp:   true,
		}
		log.SetFormatter(&f)
	}
}
