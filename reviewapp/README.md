# cactus-review ingress

This app is responsible for the /review traffic to cactus-review.

It will list available deployments to the user when visiting http://cactus-review/review

It will redirect users to http(s)://cactus-review:[assigned-port] when visiting http://cactus-review/review/[projectname]/[branchname]