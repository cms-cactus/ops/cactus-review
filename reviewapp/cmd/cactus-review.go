package main

import (
	"cactus-review/internal/logging"
	"cactus-review/internal/web/api"
	"os"

	log "github.com/sirupsen/logrus"
)

func main() {
	port := "80"
	if len(os.Args) > 1 {
		port = os.Args[1]
	}
	logging.InitLogging()
	log.Info("starting cactus-review")
	err := api.StartAndListen(port)
	if err != nil {
		log.WithError(err).Fatal("cannot start API")
	}
}
