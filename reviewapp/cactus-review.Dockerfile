# docker build -f cactus-review.Dockerfile -t gitlab-registry.cern.ch/cms-cactus/ops/cactus-review/cactus-review:$(date +%F) build

FROM scratch

ENV PATH="$PATH:/"

COPY static/kube.config build/cactus-review build/kubectl /

CMD ["cactus-review"]

EXPOSE 80