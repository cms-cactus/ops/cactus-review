module cactus-review

go 1.14

require (
	github.com/go-chi/chi v4.1.0+incompatible
	github.com/go-chi/render v1.0.1
	github.com/google/uuid v1.1.1
	github.com/sirupsen/logrus v1.5.0
	golang.org/x/crypto v0.0.0-20200406173513-056763e48d71
)
