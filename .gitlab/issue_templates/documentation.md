### What kind of documentation bug is this?

<!-- mark with [x] where applicable -->
- [ ] Missing documentation
- [ ] Incorrect documentation

### What were you trying to do?

### What kind of documentation would have been helpful?

### What level of documentation do you think would be helpful?

<!-- mark with [x] where applicable -->
- [ ] function-level code documentation
- [ ] file-level code documentation
- [ ] project-wide (markdown/twiki) documentation
- [ ] code examples

### Any existing (third party) resources that already helped?



/label ~documentation
