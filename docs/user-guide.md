# using cactus-review for your project

You will need:
- credentials (in the form of a kubectl config file)
- a docker image of your software that can accept http traffic

## getting credentials

Ask for these from one of the [maintainers](../CODEOWNERS) of this codebase.

You will receive a kube.config file, which you should treat equivalent to a plaintext password.
- never commit this file directly to your git repository  
  you could opt to store it as a CI variable, which the examples below demontstrate

These credentials will give you access to your very own namespace, making it appear as if you have an empty kubernetes cluster to your own.

Role based access policies are in place, you will not be able to do certain things like
- run privileged docker containers
- mount certain volume types

## configuring your CI

### example app

You can see a simple example 'app' in [example-app](example-app).

`.gitlab-ci.yml` will use the `Makefile` to compile the app, and use `app.Dockerfile` to create a
docker image that will be deployed with the kubernetes config in `k8s/app.yaml`.