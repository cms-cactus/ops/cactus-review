# #!/usr/bin/env bash
# set -o errexit -o nounset -o pipefail
# IFS=$'\n\t\v'

echo -n "your username: "
read TMP_USER
echo -n "your password: "
read -s TMP_PASS
echo ""

TOKEN=$(curl -i -H "Content-Type: application/json" -d "
{ \"auth\": {
    \"identity\": {
      \"methods\": [\"password\"],
      \"password\": {
        \"user\": {
          \"name\": \"$TMP_USER\",
          \"domain\": { \"id\": \"default\" },
          \"password\": \"$TMP_PASS\"
        }
      }
    },
    \"scope\": {
      \"project\": {
        \"name\": \"CMS L1 Trigger software build service\",
        \"domain\": { \"id\": \"default\" }
      }
    }
  }
}" --fail --silent "https://keystone.cern.ch/v3/auth/tokens" | grep X-Subject-Token)

if [[ "$?" -ne "0" ]]; then
  >&2 echo "invalid credentials"
  exit 1
fi

export OS_TOKEN=${TOKEN:17}
echo "export OS_TOKEN=$OS_TOKEN"
unset TMP_PASS
unset TMP_USER