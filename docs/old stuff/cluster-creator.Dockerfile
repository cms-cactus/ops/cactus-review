# docker build -f cluster-creator.Dockerfile -t gitlab-registry.cern.ch/cms-cactus/ops/cactus-review/cluster-creator:$(date +%F) .

# docker image that has all dependencies installed to use the tools in this folder
FROM cern/c8-base:20200101-1.x86_64

ENV TERRAFORM_VERSION=0.12.20
ENV GO_VERSION=1.13.6
ENV TERRAFORM_OPENSTACK_TAG=v1.25.0
ENV PATH="$PATH:/usr/local/go/bin"

RUN dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
RUN dnf install -y unzip git make ansible

# golang
RUN curl -o go.tar.gz https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz && \
    tar -C /usr/local -xzf go.tar.gz && rm -f go.tar.gz

# terraform
RUN curl -O https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip && \
    unzip terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/bin && \
    rm -f terraform_${TERRAFORM_VERSION}_linux_amd64.zip

# terraform-provider-openstack
RUN git clone https://github.com/terraform-providers/terraform-provider-openstack.git && \
    cd terraform-provider-openstack && git checkout ${TERRAFORM_OPENSTACK_TAG} && \
    make build && \
    cd .. && rm -rf terraform-provider-openstack && \
    go clean -modcache