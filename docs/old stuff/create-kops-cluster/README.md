This is an attempt to provision a K8S cluster to CERN Openstack using Kops

https://github.com/kubernetes/kops

Unfortunately, the CERN Openstack installation has a couple APIs disabled that are crucial
- cluster creation fails at the networking stage, even when instructed to use the existing network.
  this is probably because they enable the new 'neutron' api (supersedes nova) in the service catalog,
  but any endpoint times out.
- the loadbalancer doesn't work either