#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t\v'
cd `dirname "${BASH_SOURCE[0]:-$0}"`

# https://kops.sigs.k8s.io/getting_started/openstack/

#export FILENAME=cluster.yaml

if [[ -z "${OS_PROJECT_NAME:-}" ]]; then
  >&2 echo "openstack rc file not sourced. download it from openstack.cern.ch > tools > OpenStack RC File v3"
  exit 1
fi
#echo "creating ${FILENAME}"

export OS_DOMAIN_NAME=${OS_USER_DOMAIN_NAME}
export KOPS_STATE_STORE=swift://cactus-k8s-test

# openstack network list | grep CERN_NETWORK | cut -d '|' -f2

# coreos (the default) + flannel overlay cluster in Default
OS_NOVA_NETWORK=1 kops create cluster \
  --cloud openstack \
  --name cactus.k8s.local \
  --state ${KOPS_STATE_STORE} \
  --network-cidr 10.42.0.0/24 \
  --image 'C8 - x86_64 [2020-05-04]' \
  --master-count=3 \
  --os-octavia=true \
  --node-count=2 \
  --node-size m2.large \
  --master-size m2.small \
  --etcd-storage-type standard \
  --api-loadbalancer-type internal \
  --os-network d874403a-5d7e-451e-96b4-0648586355b1 \
  --topology private \
  --ssh-public-key ~/.ssh/id_rsa.pub \
  --networking weave \
  --zones cern-geneva-a,cern-geneva-b,cern-geneva-c
  # --yes
  # --zones nova \
#  --dry-run -o yaml > ${FILENAME}

# # to update a cluster
# kops update cluster cactus.k8s.local --state ${KOPS_STATE_STORE} --yes

# # to delete a cluster
# kops delete cluster cactus.k8s.local --state ${KOPS_STATE_STORE} --yes
