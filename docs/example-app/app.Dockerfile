# docker build -f app.Dockerfile -t gitlab-registry.cern.ch/cms-cactus/path/to/my/repo/app:$(date +%F) .

FROM scratch

ENV PATH="$PATH:/"

COPY build/app /

CMD ["app"]

EXPOSE 80