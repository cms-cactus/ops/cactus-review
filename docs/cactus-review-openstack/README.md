# cactus-review openstack cluster

## Terraform
[Terraform](https://www.terraform.io/) will set up our CERN OpenStack resources

You will need to first download and source the OpenStack RC file.  
You can find it in the [web interface](https://openstack.cern.ch) > `Tools` > `OpenStack RC File v3`

NOTE: since we rely on DNS loadbalancing with landb tags, which does not support liveness probes,
it is advisable to only provision one master at first, and upscale one by one.

```sh
cd terraform
# download the latest terraform state file
./tfstate-sync.sh download
# download/enable openstack plugin
terraform init
# compare state with desired state, change things if needed
# will require approval from stdin
# Nova is the 'older' network api in openstack, replaced by Neutron
# Neutron is available in the CERN Openstack service catalog, but
# not implemented, all endpoints time-out
OS_NOVA_NETWORK=1 terraform apply
# upload the new state file
./tfstate-sync.sh upload
```

## Ansible
[Ansible](https://www.ansible.com/) will SSH into the OpenStack instances and
set up a kubernetes cluster.

```sh
# (if you skipped terraform) download the ssh key
terraform/tfstate-sync.sh download

cd ansible
# run the ansible playbook
ansible-playbook -i hosts.yaml playbook.yaml
```

## Nginx
In order to not have to run the kubelets with elevated privileges, nginx is used
to forward port 80 and 443 on the host to the service IP of the installed ingress.

The ingress IP can be found like this:
```sh
kubectl get --namespace gitlab-managed-apps service ingress-nginx-ingress-controller
```
You will need to update nginx.conf to use this IP, and re-run ansible.

TODO: automate this

## removing nodes
If you wish to remove a node from the cluster, you can do so on any master node,
or on any machine with kubectl admin credentials.

```sh
# see your list of nodes
kubectl get nodes
# evict all pods from the node and mark it unschedulable
# this command will fail because you have static pods
# (to make things like networking function on the node)
kubectl drain <nodename>
# force drain any pods that are running on emptydir volumes
# or things like that
kubectl drain --delete-local-data=true --ignore-daemonsets=true <nodename>

# remove the node from the cluster
kubectl delete node <nodename>
```

You can now delete the node, if you wish to re-use it for something else you
can run `kubeadm reset`.

## running etcdctl
```sh
docker run --rm -it --net host -v /etc/kubernetes:/etc/kubernetes $(kubeadm config images list | grep etcd) sh
alias etcdctl="etcdctl --cert /etc/kubernetes/pki/etcd/peer.crt --key /etc/kubernetes/pki/etcd/peer.key --cacert /etc/kubernetes/pki/etcd/ca.crt"
etcdctl member list
```


## Upgrading the cluster
check [the upgrade docs](../upgrade-k8s.md)