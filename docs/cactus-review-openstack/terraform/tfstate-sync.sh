# #!/usr/bin/env bash
# set -o errexit -o nounset -o pipefail
# IFS=$'\n\t\v'

if [[ -z $OS_PROJECT_ID ]]; then
  >&2 echo "please source the openstack rc file first"
  exit 1
fi

if [[ -z ${OS_TOKEN:-} ]]; then
  export OS_TOKEN=$(curl -i -H "Content-Type: application/json" -d "
{ \"auth\": {
    \"identity\": {
      \"methods\": [\"password\"],
      \"password\": {
        \"user\": {
          \"name\": \"$OS_USERNAME\",
          \"domain\": { \"id\": \"default\" },
          \"password\": \"$OS_PASSWORD\"
        }
      }
    },
    \"scope\": {
      \"project\": {
        \"name\": \"CMS L1 Trigger software build service\",
        \"domain\": { \"id\": \"default\" }
      }
    }
  }
 }" --fail --silent "https://keystone.cern.ch/v3/auth/tokens" | grep X-Subject-Token | cut -b "18-" | sed 's|.$||')
  if [[ "$?" -ne "0" ]]; then
    >&2 echo "invalid credentials"
    exit 1
  fi
fi

if [[ -z ${1:-} ]]; then
  >&2 echo "please supply an 'upload' or 'download' command"
elif [[ $1 == "download" ]]; then
  curl -OH "X-Auth-Token: $OS_TOKEN" --fail https://s3.cern.ch/swift/v1/cactus-review-terraform-state/terraform.tfstate
  cat terraform.tfstate| grep private_key | cut -d "\"" -f 4 | sed "s|\\\n|\\n|g" > ssh.key
  chmod 600 ssh.key
elif [[ $1 == "upload" ]]; then
  curl -X PUT -H "X-Auth-Token: $OS_TOKEN" --fail https://s3.cern.ch/swift/v1/cactus-review-terraform-state/terraform.tfstate --upload-file terraform.tfstate
else
  >&2 echo "invalid command"
fi
