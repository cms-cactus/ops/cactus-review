locals {
  worker_nodes_todo = [for i, entry in flatten([
    for flavor, count in var.node_flavor_counts.workers : [
      for i in range(count) : {
        flavor       = flavor,
        flavor_index = i,
        id           = "${replace(flavor, ".", "-")}-${i + 1}"
      }
    ]
    ]) : {
    index         = i,
    flavor        = entry.flavor,
    flavor_index  = entry.flavor_index,
    volume_name   = "${var.cluster_name}-worker-etcd-${entry.id}"
    instance_name = "${var.cluster_name}-worker-${entry.id}"
    mount_name    = "${var.cluster_name}-worker-mount-${entry.id}"
    az            = var.az_list[i % length(var.az_list)]
  }]
}

resource "openstack_compute_instance_v2" "cactus-review-worker" {
  for_each = {
    for entry in local.worker_nodes_todo : entry.instance_name => entry
  }

  name                = each.key
  image_name          = var.base_image
  flavor_name         = each.value.flavor
  availability_zone   = each.value.az
  key_pair            = openstack_compute_keypair_v2.cactus-review.name
  stop_before_destroy = true
  config_drive        = true

  metadata = {
    landb-alias      = "${var.cluster_name}--LOAD-${each.value.index + 1}-,${var.cluster_name}-worker--LOAD-${each.value.index + 1}-"
    k8s-cluster-name = var.cluster_name
  }
}