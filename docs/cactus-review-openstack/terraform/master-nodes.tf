locals {
  master_nodes_todo = [for i, entry in flatten([
    for flavor, count in var.node_flavor_counts.masters : [
      for i in range(count) : {
        flavor       = flavor,
        flavor_index = i,
        id           = "${replace(flavor, ".", "-")}-${i + 1}"
      }
    ]
    ]) : {
    index         = i,
    flavor        = entry.flavor,
    flavor_index  = entry.flavor_index,
    volume_name   = "${var.cluster_name}-master-etcd-${entry.id}"
    instance_name = "${var.cluster_name}-master-${entry.id}"
    mount_name    = "${var.cluster_name}-master-mount-${entry.id}"
    az            = var.az_list[i % length(var.az_list)]
  }]
}

resource "openstack_blockstorage_volume_v2" "cactus-review-etcd" {
  for_each = {
    for entry in local.master_nodes_todo : entry.volume_name => entry
  }

  availability_zone = "nova"
  name              = each.key
  size              = 30

}

resource "openstack_compute_instance_v2" "cactus-review-master" {
  for_each = {
    for entry in local.master_nodes_todo : entry.instance_name => entry
  }

  name                = each.key
  image_name          = var.base_image
  flavor_name         = each.value.flavor
  availability_zone   = each.value.az
  key_pair            = openstack_compute_keypair_v2.cactus-review.name
  stop_before_destroy = true
  config_drive        = true

  metadata = {
    # do not apply the general DNS load balancing to master nodes
    # this will be used to direct traffic to services on the worker nodes
    # and thus will never be running on masters
    # landb-alias = "${var.cluster_name}--LOAD-${each.value.index + 1}-"
    landb-alias      = "${var.cluster_name}-master--LOAD-${each.value.index + 1}-"
    k8s-cluster-name = var.cluster_name
  }
}

resource "openstack_compute_volume_attach_v2" "attached" {
  for_each = {
    for entry in local.master_nodes_todo : entry.mount_name => entry
  }

  instance_id = openstack_compute_instance_v2.cactus-review-master[each.value.instance_name].id
  volume_id   = openstack_blockstorage_volume_v2.cactus-review-etcd[each.value.volume_name].id
  device      = "/dev/vdb"
}