terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.35.0"
    }
  }
}

## values for the configuration parameters here can be found by
## downloading the openrc.sh file (tools > OpenStack RC file)
# https://www.terraform.io/docs/providers/openstack/index.html#configuration-reference
provider "openstack" {
}

# note that this variable can't be used in resource names
# so you'll still see 'cactus-review' around hardcoded
variable "cluster_name" {
  description = "name of the k8s cluster"
  default     = "cactus-review-18"
}

# update ansible-hosts.yaml as well when making changes
variable "node_flavor_counts" {
  description = "map of flavors, and number of master/worker nodes to deploy with that flavor"
  default = {
    masters = {
      "m2.small" = 3
    }
    workers = {
      "m2.large" = 2
    }
  }
}

variable "az_list" {
  description = "list of availability zones to use"
  default     = ["cern-geneva-a", "cern-geneva-b", "cern-geneva-c"]
}

variable "base_image" {
  description = "base image name to use for instances"
  default     = "C8 - x86_64 [2021-05-01]"
}

resource "openstack_compute_keypair_v2" "cactus-review" {
  name = var.cluster_name
}