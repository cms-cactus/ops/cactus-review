Symptom:

```bash
[gldirkx@cactus-review ~]$ kubectl get services
NAME            TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)   AGE
cactus-review   ClusterIP   10.108.167.96   <none>        80/TCP    8d
kubernetes      ClusterIP   10.96.0.1       <none>        443/TCP   11d

[gldirkx@cactus-review ~]$ time curl 10.108.167.96
{"HTTPCode":404,"error":"Not Found"}

real  0m0.014s
user  0m0.003s
sys  0m0.009s
[gldirkx@cactus-review ~]$ 


[gldirkx@cactus-review-worker-1 ~]$ time curl 10.108.167.96
{"HTTPCode":404,"error":"Not Found"}

real  1m3.124s
user  0m0.004s
sys  0m0.007s
```

Reading material:
- https://github.com/kubernetes/kubernetes/issues/88986

In summary, we're using an old kernel with sketchy vxlan support.

proper fix: use a recent kernel, recent being less than a decade old.
temp fix: on all nodes run `sudo ethtool --offload flannel.1 rx off tx off`