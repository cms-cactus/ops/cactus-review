# Upgrading a kubernetes cluster

This guide is a tailored version of the official docs here
https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-upgrade/

# Preflight confidence checks
## check up on cluster nodes, their health, and version information  
Only proceed if all nodes have status `Ready` (and not ex: `DiskPressure` or `Ready,SchedulingDisabled`)
```console
$ kubectl get nodes
NAME                                         STATUS   ROLES    AGE   VERSION
cactus-review-18-master-m2-small-1.cern.ch   Ready    master   80d   v1.18.2
cactus-review-18-master-m2-small-2.cern.ch   Ready    master   80d   v1.18.2
cactus-review-18-master-m2-small-3.cern.ch   Ready    master   80d   v1.18.2
cactus-review-18-worker-m2-large-1.cern.ch   Ready    <none>   80d   v1.18.2
cactus-review-18-worker-m2-large-2.cern.ch   Ready    <none>   80d   v1.18.2
```
## check up on cluster core components and their health
This will show you built-in kubernetes component, and any third party core component (e.g. the CNI).  
Check that all components have status `Running`.  
Check that all components have a sane age and restart count.
```console
$ kubectl -n kube-system get pods
NAME                                                                 READY   STATUS    RESTARTS   AGE
calico-kube-controllers-789f6df884-tz9q5                             1/1     Running   0          39m
calico-node-4kspb                                                    1/1     Running   0          80d
calico-node-64t2g                                                    1/1     Running   1          80d
calico-node-cd67b                                                    1/1     Running   0          80d
calico-node-cv95w                                                    1/1     Running   0          80d
calico-node-rpxzz                                                    1/1     Running   0          80d
coredns-66bff467f8-ch2lr                                             1/1     Running   0          39m
coredns-66bff467f8-krfkv                                             1/1     Running   0          39m
etcd-cactus-review-18-master-m2-small-1.cern.ch                      1/1     Running   1          80d
etcd-cactus-review-18-master-m2-small-2.cern.ch                      1/1     Running   1          80d
etcd-cactus-review-18-master-m2-small-3.cern.ch                      1/1     Running   1          80d
kube-apiserver-cactus-review-18-master-m2-small-1.cern.ch            1/1     Running   1          80d
kube-apiserver-cactus-review-18-master-m2-small-2.cern.ch            1/1     Running   2          80d
kube-apiserver-cactus-review-18-master-m2-small-3.cern.ch            1/1     Running   3          80d
kube-controller-manager-cactus-review-18-master-m2-small-1.cern.ch   1/1     Running   591        80d
kube-controller-manager-cactus-review-18-master-m2-small-2.cern.ch   1/1     Running   618        80d
kube-controller-manager-cactus-review-18-master-m2-small-3.cern.ch   1/1     Running   627        80d
kube-proxy-9bpll                                                     1/1     Running   0          80d
kube-proxy-hhxr6                                                     1/1     Running   0          80d
kube-proxy-lfcvl                                                     1/1     Running   0          80d
kube-proxy-p2lm6                                                     1/1     Running   0          80d
kube-proxy-rwfr5                                                     1/1     Running   0          80d
kube-scheduler-cactus-review-18-master-m2-small-1.cern.ch            1/1     Running   614        80d
kube-scheduler-cactus-review-18-master-m2-small-2.cern.ch            1/1     Running   634        80d
kube-scheduler-cactus-review-18-master-m2-small-3.cern.ch            1/1     Running   623        80d
```
The scheduler looks like it's having trouble, check up on it.
```console
$ kubectl -n kube-system describe pod kube-scheduler-cactus-review-18-master-m2-small-2.cern.ch
Containers:
  kube-scheduler:
    State:          Running
      Started:      Mon, 03 Aug 2020 10:48:27 +0200
    Last State:     Terminated
      Reason:       Error
      Exit Code:    255
      Started:      Mon, 03 Aug 2020 07:27:29 +0200
      Finished:     Mon, 03 Aug 2020 10:48:26 +0200
    Ready:          True
    Restart Count:  634
    Liveness:     http-get https://127.0.0.1:10259/healthz delay=15s timeout=15s period=10s #success=1 #failure=8
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Events:
  Type     Reason            Age                      From                                                 Message
  ----     ------            ----                     ----                                                 -------
  Warning  DNSConfigForming  3m31s (x22621 over 19d)  kubelet, cactus-review-18-master-m2-small-2.cern.ch  Nameserver limits were exceeded, some nameservers have been omitted, the applied nameserver line is: 137.138.16.5 137.138.17.5 2001:1458:201:1100::5
```
The warning event is something we have to live with, the CERN CentOS provided DNS list in `/etc/resolv.conf` is indeed too big. It does however not cause these restarts.
```
$ kubectl -n kube-system logs --previous=true kube-scheduler-cactus-review-18-master-m2-small-2.cern.ch
I0803 05:27:29.503053       1 registry.go:150] Registering EvenPodsSpread predicate and priority function
I0803 05:27:29.503141       1 registry.go:150] Registering EvenPodsSpread predicate and priority function
I0803 05:27:30.068145       1 serving.go:313] Generated self-signed cert in-memory
I0803 05:27:30.885550       1 registry.go:150] Registering EvenPodsSpread predicate and priority function
I0803 05:27:30.885577       1 registry.go:150] Registering EvenPodsSpread predicate and priority function
W0803 05:27:30.887434       1 authorization.go:47] Authorization is disabled
W0803 05:27:30.887464       1 authentication.go:40] Authentication is disabled
I0803 05:27:30.887524       1 deprecated_insecure_serving.go:51] Serving healthz insecurely on [::]:10251
I0803 05:27:30.889082       1 secure_serving.go:178] Serving securely on 127.0.0.1:10259
I0803 05:27:30.889328       1 configmap_cafile_content.go:202] Starting client-ca::kube-system::extension-apiserver-authentication::requestheader-client-ca-file
I0803 05:27:30.889344       1 shared_informer.go:223] Waiting for caches to sync for client-ca::kube-system::extension-apiserver-authentication::requestheader-client-ca-file
I0803 05:27:30.889367       1 tlsconfig.go:240] Starting DynamicServingCertificateController
I0803 05:27:30.892210       1 configmap_cafile_content.go:202] Starting client-ca::kube-system::extension-apiserver-authentication::client-ca-file
I0803 05:27:30.892219       1 shared_informer.go:223] Waiting for caches to sync for client-ca::kube-system::extension-apiserver-authentication::client-ca-file
I0803 05:27:30.989548       1 leaderelection.go:242] attempting to acquire leader lease  kube-system/kube-scheduler...
I0803 05:27:30.989945       1 shared_informer.go:230] Caches are synced for client-ca::kube-system::extension-apiserver-authentication::requestheader-client-ca-file 
I0803 05:27:30.992335       1 shared_informer.go:230] Caches are synced for client-ca::kube-system::extension-apiserver-authentication::client-ca-file 
I0803 05:28:43.253092       1 leaderelection.go:252] successfully acquired lease kube-system/kube-scheduler
E0803 08:48:26.227611       1 leaderelection.go:320] error retrieving resource lock kube-system/kube-scheduler: Get https://cactus-review-18-master.cern.ch:6443/api/v1/namespaces/kube-system/endpoints/kube-scheduler?timeout=10s: context deadline exceeded
I0803 08:48:26.230956       1 leaderelection.go:277] failed to renew lease kube-system/kube-scheduler: timed out waiting for the condition
F0803 08:48:26.231036       1 server.go:244] leaderelection lost
```
When googling this, you might encounter this issue: https://bugzilla.redhat.com/show_bug.cgi?id=1769931  
In short, it is expected behavior that kube-scheduler restarts when there is a new leader election; and annoyingly, is rather dramatic about the event.

## check available versions of kubeadm for upgrade (if using the official yum repository)
This command shows you the currently installed version of kubeadm (also obtainable using `kubeadm version`);
and any previous and future versions available for install.
```console
$ dnf list --showduplicates kubeadm --disableexcludes=kubernetes
CentOS-8 - CERN                                                              198 kB/s | 3.7 kB     00:00
...
Docker CE Stable - x86_64                                                     52 kB/s | 3.5 kB     00:00
Kubernetes                                                                   443  B/s | 454  B     00:01
Installed Packages
kubeadm.x86_64          1.18.2-0           @kubernetes
Available Packages
kubeadm.x86_64          1.6.0-0            kubernetes 
...
kubeadm.x86_64          1.18.4-1           kubernetes 
kubeadm.x86_64          1.18.5-0           kubernetes 
kubeadm.x86_64          1.18.6-0           kubernetes 
```

# Summary workflow

IMPORTANT: one can only update one minor version at a time (i.e. x.y.z -> x.y+1.z).
If the installed cluster version is behind more than one minor version, this upgrade needs
to be done one minor version at a time.

Each of these steps is performed on one node at a time.  
Master nodes are to be upgraded first.

1. update kubeadm binary (`dnf upgrade yum`)
1. [optional] check upgrade plan (`kubeadm upgrade plan`)
1. drain node (`kubectl drain <node> --ignore-daemonsets`)
1. kubeadm upgrade (`kubeadm upgrade apply vx.y.z`)
1. CNI+kubectl upgrade
1. un-drain node 

# More detailed workflow

SSH into each cluster node one by one (hint: `kubectl get nodes`) and perform the following steps.  
Do master nodes first, and worker nodes last.

Be sure to preform some confidence checks from higher up this document first.

## Update kubeadm
If you installed kubeadm using yum/dnf and using the official k8s repository,
you have access to the latest binary.

```console
# dnf upgrade kubeadm
```

## Generate an upgrade plan
Kubeadm will perform checks and give an overview of the upgrade. This step is optional and if done needs only to be done on one node.

IMPORTANT: the output will call for other components that need manual update work after a kubeadm update, take note of them.
The example output asks to manually update kubelet, which is covered in this documentation.

```console
# kubeadm upgrade plan
[upgrade/config] Making sure the configuration is correct:
[upgrade/config] Reading configuration from the cluster...
[upgrade/config] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -oyaml'
[preflight] Running pre-flight checks.
[upgrade] Running cluster health checks
[upgrade] Fetching available versions to upgrade to
[upgrade/versions] Cluster version: v1.18.2
[upgrade/versions] kubeadm version: v1.18.6
[upgrade/versions] Latest stable version: v1.18.6
[upgrade/versions] Latest stable version: v1.18.6
[upgrade/versions] Latest version in the v1.18 series: v1.18.6
[upgrade/versions] Latest version in the v1.18 series: v1.18.6

Components that must be upgraded manually after you have upgraded the control plane with 'kubeadm upgrade apply':
COMPONENT   CURRENT       AVAILABLE
Kubelet     5 x v1.18.2   v1.18.6

Upgrade to the latest version in the v1.18 series:

COMPONENT            CURRENT   AVAILABLE
API Server           v1.18.2   v1.18.6
Controller Manager   v1.18.2   v1.18.6
Scheduler            v1.18.2   v1.18.6
Kube Proxy           v1.18.2   v1.18.6
CoreDNS              1.6.7     1.6.7
Etcd                 3.4.3     3.4.3-0

You can now apply the upgrade by executing the following command:

	kubeadm upgrade apply v1.18.6

_____________________________________________________________________

```

## drain node
Draining a node means marking this node as unschedulable; and thus
not scheduling any new work on this node as well as evicting existing
workloads to be run elsewhere.
This is targeted mainly for worker nodes.
If this is a master node, you should not be running any non-system components on it.
But it is good to execute this for good measure.

```console
$ kubectl drain <node> --ignore-daemonsets
node/cactus-review-18-master-m2-small-1.cern.ch cordoned
WARNING: ignoring DaemonSet-managed Pods: kube-system/calico-node-cd67b, kube-system/kube-proxy-9bpll
evicting pod kube-system/coredns-66bff467f8-qhbbk
evicting pod kube-system/calico-kube-controllers-789f6df884-cnzpv
evicting pod kube-system/coredns-66bff467f8-fgsfk
pod/calico-kube-controllers-789f6df884-cnzpv evicted
pod/coredns-66bff467f8-qhbbk evicted
pod/coredns-66bff467f8-fgsfk evicted
node/cactus-review-18-master-m2-small-1.cern.ch evicted
```

## perform kubeadm upgrade
On the first node you upgrade, execute `kubeadm upgrade apply <version>`.  
On subsequent nodes, execute `kubeadm upgrade node`.

```console
# kubeadm upgrade apply v1.18.6
[upgrade/config] Making sure the configuration is correct:
[upgrade/config] Reading configuration from the cluster...
[upgrade/config] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -oyaml'
[preflight] Running pre-flight checks.
[upgrade] Running cluster health checks
[upgrade/version] You have chosen to change the cluster version to "v1.18.6"
[upgrade/versions] Cluster version: v1.18.2
[upgrade/versions] kubeadm version: v1.18.6
[upgrade/confirm] Are you sure you want to proceed with the upgrade? [y/N]: y
[upgrade/prepull] Will prepull images for components [kube-apiserver kube-controller-manager kube-scheduler etcd]
[upgrade/prepull] Prepulling image for component etcd.
[upgrade/prepull] Prepulling image for component kube-apiserver.
[upgrade/prepull] Prepulling image for component kube-controller-manager.
[upgrade/prepull] Prepulling image for component kube-scheduler.
[apiclient] Found 0 Pods for label selector k8s-app=upgrade-prepull-etcd
[apiclient] Found 0 Pods for label selector k8s-app=upgrade-prepull-kube-scheduler
[apiclient] Found 3 Pods for label selector k8s-app=upgrade-prepull-kube-controller-manager
[apiclient] Found 3 Pods for label selector k8s-app=upgrade-prepull-kube-apiserver
[apiclient] Found 3 Pods for label selector k8s-app=upgrade-prepull-kube-scheduler
[apiclient] Found 3 Pods for label selector k8s-app=upgrade-prepull-etcd
I0803 17:18:41.043820    6660 request.go:621] Throttling request took 1.085073297s, request: GET:https://cactus-review-18-master.cern.ch:6443/api/v1/namespaces/kube-system/pods?labelSelector=k8s-app%3Dupgrade-prepull-kube-controller-manager
[upgrade/prepull] Prepulled image for component etcd.
[upgrade/prepull] Prepulled image for component kube-apiserver.
[upgrade/prepull] Prepulled image for component kube-scheduler.
[upgrade/prepull] Prepulled image for component kube-controller-manager.
[upgrade/prepull] Successfully prepulled the images for all the control plane components
[upgrade/apply] Upgrading your Static Pod-hosted control plane to version "v1.18.6"...
Static pod: kube-apiserver-cactus-review-18-master-m2-small-1.cern.ch hash: 4d358560533f404d03375c4ce0a959b6
Static pod: kube-controller-manager-cactus-review-18-master-m2-small-1.cern.ch hash: 8abf8d5dbfcd0fd812824374a1e8fc85
Static pod: kube-scheduler-cactus-review-18-master-m2-small-1.cern.ch hash: 155707e0c19147c8dc5e997f089c0ad1
[upgrade/etcd] Upgrading to TLS for etcd
[upgrade/etcd] Non fatal issue encountered during upgrade: the desired etcd version for this Kubernetes version "v1.18.6" is "3.4.3-0", but the current etcd version is "3.4.3". Won't downgrade etcd, instead just continue
[upgrade/staticpods] Writing new Static Pod manifests to "/etc/kubernetes/tmp/kubeadm-upgraded-manifests801535115"
W0803 17:19:05.747589    6660 manifests.go:225] the default kube-apiserver authorization-mode is "Node,RBAC"; using "Node,RBAC"
[upgrade/staticpods] Preparing for "kube-apiserver" upgrade
[upgrade/staticpods] Renewing apiserver certificate
[upgrade/staticpods] Renewing apiserver-kubelet-client certificate
[upgrade/staticpods] Renewing front-proxy-client certificate
[upgrade/staticpods] Renewing apiserver-etcd-client certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/kube-apiserver.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2020-08-03-17-18-54/kube-apiserver.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
Static pod: kube-apiserver-cactus-review-18-master-m2-small-1.cern.ch hash: 4d358560533f404d03375c4ce0a959b6
Static pod: kube-apiserver-cactus-review-18-master-m2-small-1.cern.ch hash: 4d358560533f404d03375c4ce0a959b6
Static pod: kube-apiserver-cactus-review-18-master-m2-small-1.cern.ch hash: 4d358560533f404d03375c4ce0a959b6
Static pod: kube-apiserver-cactus-review-18-master-m2-small-1.cern.ch hash: 4d358560533f404d03375c4ce0a959b6
Static pod: kube-apiserver-cactus-review-18-master-m2-small-1.cern.ch hash: 4d358560533f404d03375c4ce0a959b6
Static pod: kube-apiserver-cactus-review-18-master-m2-small-1.cern.ch hash: f61b8e67b4e78f3e8bdd8f6dcf87c9e5
[apiclient] Found 3 Pods for label selector component=kube-apiserver
[upgrade/staticpods] Component "kube-apiserver" upgraded successfully!
[upgrade/staticpods] Preparing for "kube-controller-manager" upgrade
[upgrade/staticpods] Renewing controller-manager.conf certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/kube-controller-manager.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2020-08-03-17-18-54/kube-controller-manager.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
Static pod: kube-controller-manager-cactus-review-18-master-m2-small-1.cern.ch hash: 8abf8d5dbfcd0fd812824374a1e8fc85
Static pod: kube-controller-manager-cactus-review-18-master-m2-small-1.cern.ch hash: 8abf8d5dbfcd0fd812824374a1e8fc85
Static pod: kube-controller-manager-cactus-review-18-master-m2-small-1.cern.ch hash: 5deecdfd2eab6ca6210855e5efe88b99
[apiclient] Found 3 Pods for label selector component=kube-controller-manager
[upgrade/staticpods] Component "kube-controller-manager" upgraded successfully!
[upgrade/staticpods] Preparing for "kube-scheduler" upgrade
[upgrade/staticpods] Renewing scheduler.conf certificate
[upgrade/staticpods] Moved new manifest to "/etc/kubernetes/manifests/kube-scheduler.yaml" and backed up old manifest to "/etc/kubernetes/tmp/kubeadm-backup-manifests-2020-08-03-17-18-54/kube-scheduler.yaml"
[upgrade/staticpods] Waiting for the kubelet to restart the component
[upgrade/staticpods] This might take a minute or longer depending on the component/version gap (timeout 5m0s)
Static pod: kube-scheduler-cactus-review-18-master-m2-small-1.cern.ch hash: 155707e0c19147c8dc5e997f089c0ad1
Static pod: kube-scheduler-cactus-review-18-master-m2-small-1.cern.ch hash: 3dd66788a2c7782d910d05ea37b91678
[apiclient] Found 3 Pods for label selector component=kube-scheduler
[upgrade/staticpods] Component "kube-scheduler" upgraded successfully!
[upload-config] Storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
[kubelet] Creating a ConfigMap "kubelet-config-1.18" in namespace kube-system with the configuration for the kubelets in the cluster
[kubelet-start] Downloading configuration for the kubelet from the "kubelet-config-1.18" ConfigMap in the kube-system namespace
[kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to get nodes
[bootstrap-token] configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials
[bootstrap-token] configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token
[bootstrap-token] configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
[addons] Applied essential addon: CoreDNS
[addons] Applied essential addon: kube-proxy

[upgrade/successful] SUCCESS! Your cluster was upgraded to "v1.18.6". Enjoy!

[upgrade/kubelet] Now that your control plane is upgraded, please proceed with upgrading your kubelets if you haven't already done so.
```

## upgrade CNI plugin
This step only needs to be executed once, after the upgrade of the first node.

This will depend on the CNI your cluster uses. In cactus-review, this is Calico.
https://docs.projectcalico.org/maintenance/kubernetes-upgrade

In the ideal case (where no pods use calico features that changed, read upgrade docs) one merely needs to apply an updated
manifest just like it was done during installation by [the ansible playbook](cactus-review-openstack/ansible/kubeadm-init.sh).  
Note the NFT patch.

Closely monitor the upgrade process. Each Calico node should update one by one until all are back in the `Running` state.

```console
$ watch --diff kubectl -n kube-system get pods
NAME                                                                 READY   STATUS        RESTARTS   AGE
calico-kube-controllers-789f6df884-tz9q5                             0/1     Terminating   0          4h49m
calico-node-4kspb                                                    1/1     Running       0          81d
calico-node-64t2g                                                    1/1     Running       1          81d
calico-node-78mdr                                                    0/1     Init:0/3      0          6s
calico-node-cd67b                                                    1/1     Running       0          81d
calico-node-cv95w                                                    1/1     Running       0          81d
coredns-66bff467f8-ch2lr                                             1/1     Running       0          4h49m
coredns-66bff467f8-krfkv                                             1/1     Running       0          4h49m
etcd-cactus-review-18-master-m2-small-1.cern.ch                      1/1     Running       1          81d
etcd-cactus-review-18-master-m2-small-2.cern.ch                      1/1     Running       1          81d
etcd-cactus-review-18-master-m2-small-3.cern.ch                      1/1     Running       1          81d
kube-apiserver-cactus-review-18-master-m2-small-1.cern.ch            1/1     Running       1          25m
kube-apiserver-cactus-review-18-master-m2-small-2.cern.ch            1/1     Running       2          81d
kube-apiserver-cactus-review-18-master-m2-small-3.cern.ch            1/1     Running       3          81d
kube-controller-manager-cactus-review-18-master-m2-small-1.cern.ch   1/1     Running       0          24m
kube-controller-manager-cactus-review-18-master-m2-small-2.cern.ch   1/1     Running       618        81d
kube-controller-manager-cactus-review-18-master-m2-small-3.cern.ch   1/1     Running       627        81d
kube-proxy-2vbqp                                                     1/1     Running       0          24m
kube-proxy-bqvc8                                                     1/1     Running       0          24m
kube-proxy-bslng                                                     1/1     Running       0          24m
kube-proxy-clccw                                                     1/1     Running       0          24m
kube-proxy-fk6s6                                                     1/1     Running       0          23m
kube-scheduler-cactus-review-18-master-m2-small-1.cern.ch            1/1     Running       0          24m
kube-scheduler-cactus-review-18-master-m2-small-2.cern.ch            1/1     Running       635        81d
kube-scheduler-cactus-review-18-master-m2-small-3.cern.ch            1/1     Running       623        81d
```

## upgrade kubectl

```console
# dnf install kubelet kubectl
# systemctl daemon-reload
# systemctl restart kubelet
```

After this command, your node should report the updated version.
```console
$ kubectl get nodes
NAME                                         STATUS                     ROLES    AGE   VERSION
cactus-review-18-master-m2-small-1.cern.ch   Ready,SchedulingDisabled   master   81d   v1.18.6
cactus-review-18-master-m2-small-2.cern.ch   Ready                      master   81d   v1.18.2
cactus-review-18-master-m2-small-3.cern.ch   Ready                      master   81d   v1.18.2
cactus-review-18-worker-m2-large-1.cern.ch   Ready                      <none>   81d   v1.18.2
cactus-review-18-worker-m2-large-2.cern.ch   Ready                      <none>   81d   v1.18.2
```

## re-activate node

```console
$ kubectl uncordon <node>
```

Your node should now be fully operational
```console
$ kubectl get nodes
NAME                                         STATUS   ROLES    AGE   VERSION
cactus-review-18-master-m2-small-1.cern.ch   Ready    master   81d   v1.18.6
cactus-review-18-master-m2-small-2.cern.ch   Ready    master   81d   v1.18.2
cactus-review-18-master-m2-small-3.cern.ch   Ready    master   81d   v1.18.2
cactus-review-18-worker-m2-large-1.cern.ch   Ready    <none>   81d   v1.18.2
cactus-review-18-worker-m2-large-2.cern.ch   Ready    <none>   81d   v1.18.2
```