# creating a k8s testing cluster on CERN openstack

## creating the openstack VM

For this, we use [terraform](https://www.terraform.io/) and its [OpenStack provider](https://www.terraform.io/docs/providers/openstack/)

The gitlab-registry.cern.ch/cms-cactus/ops/cactus-review/cluster-creator image can be used to run these commands
```bash
source get_openstack_token.sh
terraform init
terraform apply
```

## installing required software
For this, we use [Ansible](https://www.ansible.com/), which opens an SSH connection and runs a playbook.

For this, you will need the private key of the cactus-review Openstack keypair.
This is stored in a protected secret CI variable in this repo. If you are a maintainer of this project,
you have access to it.  
Store the key in private.key

The gitlab-registry.cern.ch/cms-cactus/ops/cactus-review/cluster-creator image can be used to run these commands
```bash
ansible-playbook -i ansible-hosts playbook.yaml
```

This will, among other things, install an nginx server that redirects ports 80,443 to 30000,30001.  
This way, kubernetes will not need administrator privileges to run it's ingress controller.

## creating your cluster
A very good approach would be to use [KOPS](https://github.com/kubernetes/kops).

However, while it has support for OpenStack, you will find it will not work on CERN OpenStack.
Perhaps by the time you read this, this has changed. Do try it.

The cactus-review cluster has been created with [kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/).  
This was performed on a CC7 host.

```bash
systemctl disable --now firewalld

# Install docker dependencies
yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce docker-ce-cli containerd.io

# Install kubernetes dependencies
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

# Set SELinux in permissive mode (effectively disabling it)
setenforce 0
sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes

# Enable services
systemctl enable docker && systemctl start docker
systemctl enable kubelet && systemctl start kubelet

# Required for some pod network connections
modprobe br_netfilter

cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

sysctl --system

kubeadm init --pod-network-cidr=10.244.0.0/16
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```

## securing your cluster

Read https://kubernetes.io/docs/concepts/policy/pod-security-policy/