#!/usr/bin/env bash
set -o errexit -o nounset -o pipefail
IFS=$'\n\t\v'
cd `dirname "${BASH_SOURCE[0]:-$0}"`

NAME=$1 # fails by design if no $1 set
NAMPSPACE=${2:-$NAME}
GROUP="cactus"
CA_LOCATION="/etc/kubernetes/pki"
SERVER=$(kubectl config view -o jsonpath='{.clusters[?(@.name == "kubernetes")].cluster.server}')

## create certificates
openssl genrsa -out $NAME.key 2048
openssl req -new -key $NAME.key -out $NAME.csr -subj "/CN=$NAME/O=$GROUP"
openssl x509 -req -in $NAME.csr -CA $CA_LOCATION/ca.crt \
    -CAkey $CA_LOCATION/ca.key -CAcreateserial \
    -out $NAME.crt -days 500

## create config file
kubectl config --kubeconfig $NAME.config set-credentials $NAME \
  --client-certificate=$NAME.crt  --client-key=$NAME.key --embed-certs=true

kubectl config --kubeconfig $NAME.config set-context $NAME-context \
  --cluster=kubernetes --namespace=$NAME --user=$NAME

kubectl config --kubeconfig $NAME.config set-cluster kubernetes \
  --server=$SERVER --certificate-authority=$CA_LOCATION/ca.crt --embed-certs=true

kubectl config --kubeconfig $NAME.config use-context $NAME-context

## cleanup
rm -rf $NAME.key $NAME.csr $NAME.crt