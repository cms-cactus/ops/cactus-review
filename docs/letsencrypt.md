# Letsencrypt + nginx-ingress-controller

do this one domain at a time if you haven't passed verification yet
```
certbot certonly --manual --preferred-challenges dns -d 'cmsl1test.juravenator.dev,*.cmsl1test.juravenator.dev'
```

```
cp /etc/letsencrypt/live/cmsl1test.juravenator.dev-0001/privkey.pem site.key
cp /etc/letsencrypt/live/cmsl1test.juravenator.dev-0001/cert.pem site.crt
cp /etc/letsencrypt/live/cmsl1test.juravenator.dev-0001/chain.pem ca.crt
cp /etc/letsencrypt/live/cmsl1test.juravenator.dev-0001/fullchain.pem ca2.crt
```

```
kubectl create secret tls ingress-tls --cert=site.crt --key=site.key
kubectl edit deployment nginx-ingress-nginx-ingress
```

add the following to the pod cmd arguments
```
        - -wildcard-tls-secret=default/ingress-tls
        - -default-server-tls-secret=default/ingress-tls
```
